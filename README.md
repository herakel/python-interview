# Database



Per ogni punto della lista, scrivere la query corrispondente nel rispettivo file all'interno della cartella `database/`

1. **`1-order-with-more-products.sql`** Scrivere una query che mostri l’ordine con il maggior numero di prodotti acquistati tenendo conto della quantità;
2. **`2-order-with-highest-price.sql`** Scrivere una query che estragga l’ordine con il prezzo totale più alto;
3. **`3-products-never-bought.sql`** Scrivere una query che ritorni i prodotti che non sono mai stati acquistati;

## Struttura dei dati



Di seguito è definito lo schema dei dati delle tabelle che bisognerà interrogare



### Products

First Header  | Second Header | Nullable
------------- | ------------- | -------------
id            | int           | FALSE
code          | text          | FALSE
description   | text          | FALSE
image_uri     | text          | TRUE
price         | float         | FALSE



### Orders

First Header  | Second Header | Nullable
------------- | ------------- | -------------
id            | int           | FALSE
notes         | text          | TRUE
created_at    | datetime      | FALSE



### Orders_Products

Name          | Type          | Nullable
------------- | ------------- | -------------
order_id      | int           | FALSE
product_id    | int           | FALSE
quantity      | int           | FALSE



---


# Backend

Implementare all'interno del web server fornito in `backend/app.py` le seguenti API:

1. Login e Registrazione tramite email/password
    > Bisognerà creare la tabella Users necessaria per l'autenticazione
2. Scrivere le CRUD per i prodotti:
    - Ritornare la lista dei prodotti con paginazione;
        >Usare `offset` e `count` come parametri per la paginazione
    - Inserire uno o più prodotti
    - Modifica delle informazioni di un prodotto
    - Eliminare un prodotto
3. Ritornare la lista di ordini effettuati con i relativi prodotti;
4. Ritornare la lista di ordini in formato CSV (API protetta da autenticazione con JWT)

## First-setup

```
$ python3.9 -m venv env
$ source env/bin/activate
$ pip install -r requirements.txt
$ flask run
```

## Bonus Points
1. Documentare le API tramite Swagger
2. Permettere che il progetto, sia backend che database, possa essere avviato come container Docker
3. Utilizzare i typings in Python