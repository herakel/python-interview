from flask import Flask
from dotenv import load_dotenv
import os
import mysql.connector

app = Flask(__name__)

load_dotenv()

DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')
DB_NAME = os.getenv('DB_NAME')

@app.route("/test")
def testConnection():
    connection = mysql.connector.connect(host=DB_HOST, port=DB_PORT, user=DB_USER, password=DB_PASS)
    cur = connection.cursor()

    cur.execute("SELECT CURDATE()")
    row = cur.fetchone()
    response = "Current date is: {0}".format(row[0])

    connection.close()
    return response

@app.route("/login", methods=['POST'])
def login():
    pass

@app.route("/register", methods=['POST'])
def register():
    pass

@app.route("/products", methods=['GET'])
def get_products():
    pass

@app.route("/products/<id>", methods=['GET'])
def get_product(id):
    pass

@app.route("/products", methods=['POST'])
def add_product():
    pass

@app.route("/products/<id>", methods=['PUT'])
def update_product(id):
    pass

@app.route("/products/<id>", methods=['DELETE'])
def delete_product(id):
    pass

@app.route("/orders", methods=['GET'])
def get_orders():
    pass

@app.route("/orders-csv", methods=['GET'])
def get_orders_csv():
    pass